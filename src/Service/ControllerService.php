<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ControllerService.
 */
class ControllerService
{
    /**
     * @var PaginatorInterface
     */
    public $paginator;

    /**
     * @var EntityManagerInterface
     */
    public $entityManger;

    /**
     * ControllerService constructor.
     */
    public function __construct(PaginatorInterface $paginator, EntityManagerInterface $entityManager)
    {
        $this->paginator = $paginator;
        $this->entityManger = $entityManager;
    }

    public function formHandle(Request $request, FormInterface $form)
    {
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->persistEntity($form->getData());

                return true;
            }
        }

        return false;
    }

    /**
     * @param $page
     * @param $perPage
     * @param $data
     *
     * @return PaginationInterface
     */
    public function showAll($page, $perPage, $data)
    {
        if (!$page || 0 === $page) {
            $page = 1;
        }

        if (!$perPage || 0 === $perPage) {
            $perPage = 10;
        }

        $pagination = $this->paginator->paginate(
            $data,
            $page,
            $perPage
        );

        return $pagination;
    }

    protected function persistEntity($entity)
    {
        $this->entityManger->persist($entity);
        $this->entityManger->flush();
    }
}
