<?php

namespace App\Form\Type;

use App\Entity\Participant;
use App\Entity\TravelCost;
use App\Entity\TravelType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ParticipantFormType.
 */
class ParticipantFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, [
                'required' => true,
                'attr' => ['maxlength' => 255],
                'label' => 'Imię',
            ])
            ->add('lastName', TextType::class, [
                'required' => true,
                'attr' => ['maxlength' => 255],
                'label' => 'Nazwisko',
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'E-mail',
            ])
            ->add('travelCost', EntityType::class, [
                'required' => true,
                'class' => TravelCost::class,
                'expanded' => true,
                'label' => 'Koszt',
            ])
            ->add('travelType', EntityType::class, [
                'required' => true,
                'class' => TravelType::class,
                'label' => 'Typ biletu',
            ])
            ->add('AdditionalInfo', TextareaType::class, [
                'label' => 'Dodatkowe informacje',
                'attr' => ['maxlength' => 255],
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Participant::class,
        ]);
    }
}
