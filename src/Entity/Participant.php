<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Participant
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity="TravelCost")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $travelCost;

    /**
     * @ORM\ManyToOne(targetEntity="TravelType")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $travelType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $additionalInfo;

    public function getId(): int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @return Participant
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @return Participant
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return Participant
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTravelCost(): ?TravelCost
    {
        return $this->travelCost;
    }

    /**
     * @return Participant
     */
    public function setTravelCost(TravelCost $travelCost): self
    {
        $this->travelCost = $travelCost;

        return $this;
    }

    public function getTravelType(): ?TravelType
    {
        return $this->travelType;
    }

    /**
     * @return Participant
     */
    public function setTravelType(TravelType $travelType): self
    {
        $this->travelType = $travelType;

        return $this;
    }

    public function getAdditionalInfo(): ?string
    {
        return $this->additionalInfo;
    }

    /**
     * @return Participant
     */
    public function setAdditionalInfo(string $additionalInfo): self
    {
        $this->additionalInfo = $additionalInfo;

        return $this;
    }
}
