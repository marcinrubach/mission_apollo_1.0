<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200313024220 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE participant (
          id INT AUTO_INCREMENT NOT NULL, 
          travel_cost_id INT DEFAULT NULL, 
          travel_type_id INT DEFAULT NULL, 
          first_name VARCHAR(255) NOT NULL, 
          last_name VARCHAR(255) NOT NULL, 
          email VARCHAR(255) NOT NULL, 
          additional_info VARCHAR(255) DEFAULT NULL, 
          INDEX IDX_D79F6B11DCFC326E (travel_cost_id), 
          INDEX IDX_D79F6B1140F3B82 (travel_type_id), 
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE travel_cost (
          id INT AUTO_INCREMENT NOT NULL, 
          name VARCHAR(255) NOT NULL, 
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE travel_type (
          id INT AUTO_INCREMENT NOT NULL, 
          name VARCHAR(255) NOT NULL, 
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE 
          participant 
        ADD 
          CONSTRAINT FK_D79F6B11DCFC326E FOREIGN KEY (travel_cost_id) REFERENCES travel_cost (id)');
        $this->addSql('ALTER TABLE 
          participant 
        ADD 
          CONSTRAINT FK_D79F6B1140F3B82 FOREIGN KEY (travel_type_id) REFERENCES travel_type (id)');


        $this->addSql('INSERT INTO `travel_cost` VALUES (1,\'0-10 tyś.\')');
        $this->addSql('INSERT INTO `travel_cost` VALUES (2,\'10-50 tyś.\')');
        $this->addSql('INSERT INTO `travel_cost` VALUES (3,\'50-100 tyś.\')');

        $this->addSql('INSERT INTO `travel_type` VALUES (1,\'Tylko na Marsa.\')');
        $this->addSql('INSERT INTO `travel_type` VALUES (2,\'W obie strony.\')');


        $this->addSql('INSERT INTO `participant` VALUES (1, 1, 1,\'Jan\', \'Kowalski\', \'jan.kowalski@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (2, 2, 2,\'Jeremi\', \'Kąkolewski\', \'Jeremi.Kąkolewski@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (3, 3, 1,\'Maksymilian\', \'Tchórzewski\', \'Maksymilian.Tchórzewski@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (4, 2, 2,\'Damian\', \'Majcherek\', \'Damian.Majcherek@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (5, 2, 1,\'Ignacy\', \'Wiśniewski\', \'Ignacy.Wiśniewski@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (6, 1, 2,\'Franciszek\', \'Kopański\', \'Franciszek.Kopański@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (7, 2, 2,\'Kacper\', \'Idziak\', \'Kacper.Idziak@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (8, 2, 2,\'Bartek\', \'Sala\', \'Bartek.Sala@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (9, 3, 1,\'Marcin\', \'Gulczyński\', \'Marcin.Gulczyński@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (10, 3, 1,\'Stanisław\', \'Błaszak\', \'Stanisław.Błaszak@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (11, 3, 2,\'Michał\', \'Pasierb\', \'Michał.Pasierb@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (12, 2, 2,\'Hubert\', \'Krzysztoń\', \'Hubert.Krzysztoń@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (13, 2, 2,\'Antoni\', \'Palacz\', \'Antoni.Palacz@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (14, 1, 2,\'Maciej\', \'Garstka\', \'Maciej.Garstka@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (15, 2, 2,\'Antoni\', \'Bartosiewicz\', \'Antoni.Bartosiewicz@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (16, 1, 1,\'Jan\', \'Ożga\', \'jan.Ożga@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (17, 1, 1,\'Damian\', \'Oleszko\', \'Damian.Oleszko@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (18, 3, 2,\'Bartek\', \'Kowalski\', \'Bartek.kowalski@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (19, 2, 2,\'Hubert\', \'Miotk\', \'Hubert.Miotk@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (20, 1, 2,\'Jan\', \'Strączek\', \'jan.Strączek@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (21, 1, 2,\'Maksymilian\', \'Antos\', \'Maksymilian.Antos@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (22, 3, 2,\'Jan\', \'Kowalski\', \'jan.kowalski@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (23, 2, 1,\'Michał\', \'Miotk\', \'Michał.Miotk@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (24, 2, 1,\'Robert\', \'Kowalski\', \'Robert.kowalski@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (25, 1, 2,\'Jan\', \'Antos\', \'jan.Antos@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (26, 1, 2,\'Kacper\', \'Kowalski\', \'Kacper.kowalski@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (27, 3, 2,\'Bartek\', \'Strączek\', \'Bartek.Strączek@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (28, 3, 2,\'Stanisław\', \'Przywara\', \'Stanisław.Przywara@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (29, 3, 2,\'Maciej\', \'Kowalski\', \'Maciej.kowalski@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (30, 1, 1,\'Michał\', \'Oleszko\', \'Michał.Oleszko@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (31, 2, 2,\'Marcin\', \'Kowalski\', \'Marcin.kowalski@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (32, 2, 1,\'Hubert\', \'Antos\', \'Hubert.Antos@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (33, 1, 2,\'Maciej\', \'Kowalski\', \'Maciej.kowalski@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (34, 3, 1,\'Franciszek\', \'Sulej\', \'Franciszek.Sulej@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (35, 2, 2,\'Jan\', \'Przywara\', \'jan.Przywara@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (36, 1, 2,\'Antoni\', \'Ożga\', \'Antoni.Ożga@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (37, 2, 2,\'Stanisław\', \'Kowalski\', \'Stanisław.kowalski@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (38, 2, 1,\'Jan\', \'Kowalski\', \'jan.kowalski@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (39, 3, 1,\'Robert\', \'Bartosiewicz\', \'Robert.Bartosiewicz@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (40, 3, 2,\'Franciszek\', \'Kowalski\', \'Franciszek.kowalski@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (41, 1, 2,\'Robert\', \'Przywara\', \'Robert.Przywara@mail.com\', \'...\')');
        $this->addSql('INSERT INTO `participant` VALUES (42, 1, 1,\'Stanisław\', \'Sulej\', \'Stanisław.Sulej@mail.com\', \'...\')');

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE participant DROP FOREIGN KEY FK_D79F6B11DCFC326E');
        $this->addSql('ALTER TABLE participant DROP FOREIGN KEY FK_D79F6B1140F3B82');
        $this->addSql('DROP TABLE participant');
        $this->addSql('DROP TABLE travel_cost');
        $this->addSql('DROP TABLE travel_type');
    }
}
