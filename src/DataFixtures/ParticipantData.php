<?php

namespace App\DataFixtures;

use App\Entity\Participant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ParticipantData extends Fixture implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        $this->createFixture(
            $manager,
            'Jan',
            'Kowalski',
            'travelCost_0-10',
            'jan.kowalski@mail.com',
            'travelType_bothSides',
            'Dodatkowe infomacje'
        );

        $this->createFixture(
            $manager,
            'Jakub',
            'Nowicki',
            'travelCost_10-50',
            'jakub.nowicki@mail.com',
            'travelType_onlyMars',
            'Dodatkowe infomacje'
        );
        $manager->flush();
    }

    /**
     * @param $firstName
     * @param $lastName
     * @param $cost
     * @param $email
     * @param $travelType
     * @param $additionalInfo
     * @param $reference
     */
    public function createFixture(
        ObjectManager $manager, $firstName, $lastName, $travelCost, $email, $travelType, $additionalInfo = ''
    ) {
        $fixture = new Participant();
        $fixture->setFirstName($firstName);
        $fixture->setLastName($lastName);
        $fixture->setTravelCost($this->getReference($travelCost));
        $fixture->setEmail($email);
        $fixture->setTravelType($this->getReference($travelType));
        $fixture->setAdditionalInfo($additionalInfo);
        $manager->persist($fixture);
    }

    /**
     * Get the order of this fixture.
     *
     * @return int
     */
    public function getOrder()
    {
        return 2;
    }
}
