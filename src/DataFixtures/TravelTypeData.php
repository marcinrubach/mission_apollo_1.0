<?php

namespace App\DataFixtures;

use App\Entity\TravelType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Class TravelTypeData.
 */
class TravelTypeData extends Fixture implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        $this->createFixture($manager, 'W dwie strony.', 'travelType_bothSides');
        $this->createFixture($manager, 'Tylko na Marsa.', 'travelType_onlyMars');
        $manager->flush();
    }

    /**
     * @param $name
     * @param $reference
     */
    public function createFixture(ObjectManager $manager, $name, $reference)
    {
        $fixture = new TravelType();
        $fixture->setName($name);
        $this->setReference($reference, $fixture);
        $manager->persist($fixture);
    }

    /**
     * Get the order of this fixture.
     *
     * @return int
     */
    public function getOrder()
    {
        return 1;
    }
}
