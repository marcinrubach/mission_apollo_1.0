<?php

namespace App\DataFixtures;

use App\Entity\TravelCost;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TravelCostData extends Fixture implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        $this->createFixture($manager, '0-10 tyś.', 'travelCost_0-10');
        $this->createFixture($manager, '10-50 tyś.', 'travelCost_10-50');
        $this->createFixture($manager, '50-100 tyś.', 'travelCost_50-100');
        $manager->flush();
    }

    /**
     * @param $name
     * @param $reference
     */
    public function createFixture(ObjectManager $manager, $name, $reference)
    {
        $fixture = new TravelCost();
        $fixture->setName($name);
        $this->setReference($reference, $fixture);
        $manager->persist($fixture);
    }

    /**
     * Get the order of this fixture.
     *
     * @return int
     */
    public function getOrder()
    {
        return 1;
    }
}
