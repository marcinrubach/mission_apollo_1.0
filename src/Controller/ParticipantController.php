<?php

namespace App\Controller;

use App\Entity\Participant;
use App\Form\Type\ParticipantFormType;
use App\Service\ControllerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ParticipantController.
 */
class ParticipantController extends AbstractController
{
    /**
     * @var ControllerService
     */
    public $service;

    /**
     * ParticipantController constructor.
     */
    public function __construct(ControllerService $service)
    {
        $this->service = $service;
    }

    /**
     * @Route("/", name="participant_list")
     */
    public function listView(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Participant::class);
        $page = 1;

        if ($request) {
            $page = $request->query->getInt('page', 1);
        }

        return $this->render('participant/list.html.twig', [
            'pagination' => $this->service->showAll(
                $page, 20, $repository->findAll()
            ),
        ]);
    }

    /**
     * @Route("/new", name="participant_new")
     *
     * @return Response
     */
    public function addView(Request $request)
    {
        $task = new Participant();
        $form = $this->createForm(ParticipantFormType::class, $task);

        if ($this->service->formHandle($request, $form)) {
            $this->addFlash('success', 'Pomyślnie wysłano zgłoszenie');
        }

        return $this->render('participant/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
